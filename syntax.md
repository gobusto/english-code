SPELLING - Simple Pseudo English Language Logic Including Natural Grammar
=========================================================================

This document outlines a game scripting language designed to allow simple logic
to be described using a limited subset of the English language.

History
-------

+ 2015-07-25: Initial draft by Thomas Glyn Dennis
+ 2018-12-27: Change string delimitation rules.

General syntax
--------------

All "tokens" are separated by whitespace characters (such as tabs or newlines).
Newlines are not "special" in any way, and indentation does not have any effect
on the program; think C or Java, rather than Python or CoffeeScript.

Upper- and lower-case characters are generally interchangeable, though `STRING`
and `string` are not considered to be equal when compared as strings.

Punctuation is generally ignored (outside of strings), but with two exceptions:

+ 'Single' or "double" quotes (or `backticks`) can be used to  delimit strings.
+ Parentheses (i.e. the `(` and `)` characters) are used for comment blocks. As
  such, they only "matter" *inside* strings, such as `"An (old) bucket"`.

Note that strings without spaces do not require delimiters, unless they need to
start with a punctuation character:

+ `Hello!` does not require delimiters because it doesn't contain whitespace.
+ `"Hello World!"` requires delimiters because it contains a space.
+ `"!olleH"` requires delimiters because it starts with a punctuation mark.

Procedures
----------

Procedures are defined using `FOR name`, where `name` is a string. Their "body"
is implicitly ended by another `FOR` statement or end-of-file.

Each procedure is expected to correspond to a specific type of "event" (defined
by the game interpreting the script), so an example file may look like this:

    for update
      (...handle AI logic...)

    for injury
      (...decrease health...)

    for death
      (...spawn some bonuses for the player to pick up...)

They are not referred to as "functions" since they do not specify parameters or
return values. Instead, one or more "global" objects are expected to exist when
they run, so that a procedure can read or update their fields (and hence change
their state).

Objects and Attributes
----------------------

Objects are referred to using a name string, which may contain spaces, or start
with a number; most other programming languages do not allow this, but `Steve`,
`"Steve Smith"`, `"123"`, or even `":-)"` are all valid object names.

Objects have one or more attributes, which have the same name rules as objects.

The only object *guaranteed* to exist when a procedure runs is `MY` (equivalent
to `self` or `this` in other programming languages). Other objects will usually
be available, but this depends on (A) the game interpreting the script, and (B)
the type of event that the procedure is "reacting" to.

For example, a "collide" event will usually have at least two objects: `MY` and
`THEIR`, each of which might have a list of attributes describing their current
state: `my health`, `their cash`, or `my "favourite colour"` are all possible.

The underlying datatype of an attribute will depend on the interpreter; scripts
categorise values as either a number (integer or real) or a string of text.

Verbs
-----

Object attributes can be manipulated in various ways. The general syntax is:

+ `VERB`
+ `THE OBJECT ATTRIBUTE`, where `THE` is optional.
+ `XXX`, which may be `BY`, `TO`, or `FROM` depending on context.
+ `LITERAL` or `OBJECT ATTRIBUTE`

Some verbs can be used with both numbers and strings, but some can only be used
with a specific type.

### Numbers

Numbers

    INCREASE  my     stength      BY 2
    DECREASE  player health       BY my strength
    DIVIDE    my     speed        BY 2
    MULTIPLY  my     intelligence BY their intelligence
    MODULO    my     health       BY 16
    RANDOMISE their  health       BY 8

Note that `RANDOMISE` picks a random number between 1 and N (not 0 and N-1), so
it acts like an N-sided die.

If an object name begins with a numeric character, then it must be delimited in
some way to disambiguate it from a plain number:

    set 8object 2attribute to "5object" 4attribute

Alternative spellings/keywords are available for some operations:

    INCREMENT my stength BY 2
    DECREMENT my luck    BY 4
    RANDOMIZE my health  BY 8

There are also a couple of special cases where a different pattern can be used:

    ADD      5 TO   my health
    SUBTRACT 5 FROM my luck

Omitting `BY ...` after `INCREMENT`/`DECREMENT`/`INCREASE`/`DECREASE` assumes
that a value of `1` should be used: `increment my luck` adds 1 to `my luck`.

The same general pattern is used to set a property to a specific value but `BY`
is replaced with `TO`:

    SET    my health TO 100
    UPDATE my health TO 100
    CHANGE my health TO 100

Using `TO` instead of `BY` for `INCREMENT`/`DECREMENT`/`INCREASE`/`DECREASE`
causes them to behave as though they were `SET`/`UPDATE`/`CHANGE`, so that
`INCREASE my luck TO 10` sets luck to 10 instead of adding to it.

### Strings

It's also possible to set string values this way:

    SET    my          name TO Boris
    UPDATE his         name TO "Steve Jones"
    CHANGE the players name TO Trevor

Note that the second parameter is ALWAYS a string literal, since it is not
possible to differentiate between a string representing an object name and a
string representing general text in this case. To copy a string attribute, use
`COPY` instead: `COPY my name FROM their name`

Conditions
----------

Conditions may use any of the following comparison operators:

    my health IS LESS THAN 3
    my health IS MORE THAN his health
    my health IS EQUAL TO  the players intelligence

Alternative names are available:

    their health IS FEWER THAN   4
    my    health IS GREATER THAN 3
    his   health IS IDENTICAL TO my intelligence
    his   health IS THE SAME AS  my intelligence

If the keyword after `IS` is unrecognised, it's interpreted as "IS EQUAL TO" so
that `my name is Steve` can be used, instead of `my name is the same as Steve`.

The word `ARE` may be used in place of `IS`, so `my eyes ARE closed` is valid.

(TODO: Allow `IS ROUGHLY/ESSENTIALLY/BASICALLY` for case-insensitive checking?)

Strings are compared alphabetically, as per the C `strcmp()` function.

Conditions begin with IF or UNLESS:

    IF my intelligence is less than his intelligence
      ...

Additional conditions can be included with AND:

    IF my gold is more than 5 AND my luck is less than 4
      ...

Alternative conditions can be specified with OR:

    UNLESS my name is Bill AND his name is Ted
        OR my name is Bill AND his name is Ben
      ...

Note that OR starts a new (alternative) condition, whereas AND builds upon what
is already there.

Conditional sections can be terminated in one of three ways. Firstly, it is
assumed that a new FOR section should implicitly "close" any open conditions:

    for lava
      IF my "fire resistance" (from an equipped accessory) is less than 20
        decrease my health by 10

    FOR ice
      set my name to snowman

More usually, a conditional section is explicitly ended with a keyword, such as
`ANYWAY` (TODO: Add more keywords?):

    for lava
      IF my "fire resistance" is less than 20
        decrease my health by 10
      ANYWAY
      set my name to "Firey Jim"

One other possibility is to use the keyword "otherwise", which acts like `else`
does in other progamming languages:

    for lava
      IF my "fire resistance" is less than 20
        decrease my health by 10
      OTHERWISE
        decrease my health by 1
      ANYWAY
      set my name to "Firey Jim"

Miscellaneous
-------------

### Stop

The `STOP` keyword can be used to "end" a function prematurely:

    for medusa
      if my eyes are closed
        stop
      anyway
      change my body to stone

### See

The `SEE` keyword can be used like `import` or `include` in other languages; it
specifies another file to "copy in" to the current script:

    see basics/general_behaviour.txt (handles stuff like walking, eating, etc.)

    for sleep
      increase my health

Unlike most other keywords (except `FOR`) which must occur *inside* a function,
`SEE` must occur *outside* a function.

### Ignored words

The following words are ignored if they occur where a new "instruction" usually
begins: `AND`, `THEN`, `BUT`, and `HOWEVER`

    for sleep
      randomise my result by 6
      and
      if my result is more than 4
        change my state to awake
        and
        then
        update my sleepiness to 0
        but
      anyway
      increase my health

NOTE: The `AND` keyword is not ignored when it occurs as part of an `UNLESS` or
`IF` condition because it is occurring after the "start" of an instruction.

Missing features
----------------

It is not possible to define a loop, though a game may decide to call a general
"update" function whenever the main loop runs:

    for update
      if my health is less than 100
        add 1 to my health
      otherwise
        set my health to 100

Similarly, it's not possible to have one function call another, though this can
be simulated if the interpreter specifies a "next action" field of some kind:

    for injury
      if the injury cause is Steve
        set my reaction to anger

    for anger
      increase my rage
      if my rage is more than my tolerance
        set my reaction to murder
        set my victim to Steve

This would also provide another way to implement loops:

    for searching
      randomise my discovery by 2
      if my discovery is 2
        increase my food
      otherwise
        increase my gold
      anyway
      if my "discovery count" is less than 10
        decrement my "discovery count"
        set my "next action" to searching

There is currently no support for arrays. This is something that might be added
in the future.

Syntactic shortcomings
----------------------

There are a few instances where the syntactic requirements of the language lead
to slightly strange sentences, or an inability to handle certain phrasings that
are perfectly valid in natural English.

### Post-conditional IF or UNLESS statements

Unlike Ruby, it's not possible to specify conditions *after* actions:

    # This Ruby code...
    if @name == "Stephen"
      @name = "Steve"
    end
    @age = 28

    # ...could also be written as:
    @name = "Steve" if @name == "Stephen"
    @age = 28

This is because ALL whitespace (including newlines) is treated identically - so
the code would be ambiguous:

    (This works OK...)
    if my name is Stephen
      change my name to Steve
    anyway
    set my age to 28

    (...but this won't do what you expect it to...)
    change my name to Steve if my name is Stephen
    set my age to 28

    (...because it is actually interpreted like this:)
    change my name to Steve (This will ALWAYS happen...)
    if my name is Stephen
      set my age to 28  (...so this will NEVER happen!)

It may be possible to address this using punctation characters or keyword(s) in
the future, but for now it's assumed that a condition always precedes actions.

License
=======

This document covers language behaviour, rather than a specific implementation.
As such, it is placed in the public domain so that interested parties may refer
to it freely and without restriction.

See the [Creative Commons 0](http://creativecommons.org/publicdomain/zero/1.0/)
licence for further details.
